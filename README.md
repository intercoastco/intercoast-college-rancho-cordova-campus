The central mission of InterCoast Colleges is to provide associates degrees and certificate programs for careers in allied health, business and skilled trade industries and prepare students to meet employer expectations for entry level employment.

Address: 9738 Lincoln Village Dr, #120, Sacramento, CA 95827

Phone: 916-714-5400
